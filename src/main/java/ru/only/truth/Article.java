package ru.only.truth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Article {
    private String content;

    private String userName;
    private String pasportSelfie1;
    private String pasportSelfie2;
    private String phone;
    private String email;
    private String address;
    private Boolean acceptedRules;


    public String getBrief() {
        return content.length() > 61 ? content.substring(0, 60) + "..." : content;
    }
}
