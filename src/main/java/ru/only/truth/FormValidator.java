package ru.only.truth;

import org.zkoss.bind.Property;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;

import java.util.Map;

public class FormValidator extends AbstractValidator {

    public void validate(ValidationContext ctx) {
        Map<String, Property> beanProps = ctx.getProperties(ctx.getProperty().getBase());
        validateCaptcha(ctx, (String) ctx.getValidatorArg("captcha"), (String) ctx.getValidatorArg("captchaInput"));
        notEmpty(ctx, "address", (String) beanProps.get("address").getValue());
        final Object phone = beanProps.get("phone").getValue();
        notEmpty(ctx, "phone", phone instanceof Integer ? phone.toString() : (String) phone);
        notEmpty(ctx, "userName", (String) beanProps.get("userName").getValue());
        notEmpty(ctx, "email", (String) beanProps.get("email").getValue());
        notEmpty(ctx, "content", (String) beanProps.get("content").getValue());
    }

    private void validateCaptcha(ValidationContext ctx, String captcha, String captchaInput) {
        if (captchaInput == null || !captcha.equals(captchaInput)) {
            this.addInvalidMessage(ctx, "captcha", "А не совпало!");
        }
    }

    private void notEmpty(ValidationContext ctx, String key, String value) {
        if (value == null || value.isEmpty()) {
            this.addInvalidMessage(ctx, key, "Не должно быть пустым!");
        }
    }
}

