package ru.only.truth;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

@Data
@EqualsAndHashCode(callSuper = false)
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class FormViewModel extends UserForm {
    private String dateFormat;
    private int memoHeight = 6;
    private String foregroundColour = "#000000";
    private String backgroundColour = "#FDC966";

    @WireVariable("repo")
    private Repository repo;

    @Command
    @NotifyChange("memoHeight")
    public void changeMemoHeight(@ContextParam(ContextType.TRIGGER_EVENT) InputEvent change) {
        try {
            int parsed = Integer.parseInt(change.getValue());
            if (parsed > 0) {
                this.memoHeight = parsed;
            }
        } catch (NumberFormatException nfe) {
        }
    }

    @Command
    @NotifyChange("captcha")
    public void regenerate() {
        this.regenerateCaptcha();
    }

    @Command
    public void submit() {
        repo.add(getArticle());
        BindUtils.postGlobalCommand(null, null, "refreshArticles", null);
    }
}
