package ru.only.truth;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.core.config.Configurator;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@SpringBootApplication
@Log4j2
public class GiveMe implements CommandLineRunner {

    public static void main(String[] args) {
        final File log4j = new File("config/log4j2.xml");
        if (log4j.exists()) {
            Configurator.initialize("onlytruth", log4j.getAbsolutePath());
        }
        try {
            prepareConfig();
            SpringApplication.run(GiveMe.class, args);
        } catch (IOException ex) {
            log.error("Config prepare fail.", ex);
            log.throwing(ex);
        }
    }

    @Override
    public void run(String... args) {
        log.info("Only truth was started successfully\nGO for truth\nGO for truth\nGO for truth\nGO for truth");
    }

    private static void prepareConfig() throws IOException {
        File dir = new File("config");
        if (!dir.exists() || !dir.isDirectory()) {
            log.info("Create config directory");
            Files.createDirectory(dir.toPath());
        }
    }

}
