package ru.only.truth;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

public class InputDialogController extends SelectorComposer<Component> {
    private static final long serialVersionUID = 277457848L;

    @Wire
    Window modalDialog;

    @Listen("onClick = #closeBtn")
    public void closeModal(Event e) {
        modalDialog.detach();
    }

    @Listen("onClick = #saveBtn")
    public void saveModal(Event e) {
        modalDialog.detach();
    }
}
