package ru.only.truth;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

@Log4j2
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class OnlyTruthForm {

    @WireVariable("ctx")
    private Context context;

    @Getter
    @WireVariable("repo")
    private Repository repo;

    private Window window;

    @Command
    @NotifyChange("repo")
    public void add() {
        window = (Window) Executions.createComponents("~./input_dialog.zul", null, null);
        window.doModal();
    }

    @GlobalCommand("refreshArticles")
    @NotifyChange("repo")
    public void refreshArticles(){
        window.detach();
    }

    @Command
    @NotifyChange("repo")
    public void deleteAll() {
        repo.deleteAll();
    }

    @Command
    @NotifyChange("repo")
    public void remove(@BindingParam("article") Article article) {
        repo.delete(article);
    }

    @Command("read")
    public void read(@BindingParam("article") Article article) {
        context.setArticle(article);
        Window windowRead = (Window) Executions.createComponents("~./read_dialog.zul", null, null);
        windowRead.doModal();
    }
}
