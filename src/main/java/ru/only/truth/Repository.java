package ru.only.truth;

import lombok.Getter;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

@Component("repo")
@Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Repository {

    @Getter
    private final List<Article> articles = new ArrayList<>();

    public Repository() {
        initData();
    }

    private void initData() {
        if (!articles.isEmpty()) {
            articles.clear();
        }
        /*
        articles.add(Article.builder().content("1zk Spreadsheet RC Released Check this out1zk Spreadsheet RC Released Check this out1zk Spreadsheet RC Released Check this out").build());
        articles.add(Article.builder().content("2zk Spreadsheet RC Released Check this out").build());
        articles.add(Article.builder().content("3zk Spreadsheet RC Released Check this out").build());
         */
    }

    public void deleteAll() {
        articles.clear();
    }

    public void add(Article article) {
        articles.add(article);
    }

    public void delete(Article article) {
        articles.remove(article);
    }

    public int getSize() {
        return articles.size();
    }
}
