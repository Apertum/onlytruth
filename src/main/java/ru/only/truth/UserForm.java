package ru.only.truth;

import lombok.Data;

@Data
public class UserForm {
    private final RandomStringGenerator rsg = new RandomStringGenerator(2);

    private Article article = new Article();
    private String retypedPassword;
    private String captcha = rsg.getRandomString();
    private String captchaInput;

    public void regenerateCaptcha() {
        this.captcha = rsg.getRandomString();
    }
}
